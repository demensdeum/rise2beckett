from os import walk,sys
import json

projectFile = open("rise2-project.json", "r")
projectFileContent = projectFile.read()
projectFileJson = json.loads(projectFileContent)
projectFile.close()

try:
    dependencies = projectFileJson["dependencies"]
except:
    dependencies = []

outputFile = open("rise2-dependencies.hpp", "w") 

includes = [
    "iostream",
    "memory"
]

if "OpenGL" in dependencies:
    includes.append("GLES3/gl3.h")

if "SDL2" in dependencies:
    includes.append("SDL2/SDL.h")
    includes.append("SDL2/SDL_syswm.h")

if "bgfx" in dependencies:
    includes.append("bx/bx.h")
    includes.append("bgfx/bgfx.h")
    includes.append("bx/thread.h")
    includes.append("bgfx/platform.h")
    includes.append("bx/spscqueue.h")
    outputFile.write("#define BX_CONFIG_DEBUG 1\n")

for include in includes:
    outputFile.write(f"#include <{include}>\n")

for root, dirs, files in walk(sys.argv[1]):
    for file in files:
        print(f"file: {file}")
        if file != "main.ri2" and \
            file != "rise2-dependencies.hpp" and \
            file.endswith(".ri2"):
            fileName = file.split(".")[0]
            outputFile.write(f"#include \"{fileName}.hpp\"\n")
    break

outputFile.close()